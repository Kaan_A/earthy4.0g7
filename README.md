# Forum Creatio - EARTHY4.0G7

This repository contains the complete documentation of Forum Creatio EARTHY4.0 as of 02/11/2021.  
For every stage of the design the process as well as the results are documented.  
Feel free to contact any of us if you have any questions or feedback!  
  
- Maryam Aboueimehrizi (m.aboueimehrizi@student.tudelft.nl)
- Kaan Akbaba (k.akbaba@student.tudelft.nl)
- Qinglu Chen (q.chen-11@student.tudelft.nl)
- Baolian Liu (b.liu-11@student.tudelft.nl)
- Kevin Winiarczyk (k.p.a.winiarczyk@student.tudelft.nl)