# Made by Kaan Akbaba
# Last revised: 02/11/2021

import pygame
import sys
import math
import csv
import os
from pathlib import Path

from pygame import draw

#define colours
BLACK = (0, 0, 0)
WHITE = (200, 200, 200)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 100, 255)
PURPLE = (230, 0, 230)
ORANGE = (255,100,0)
CYAN = (0, 230, 230)
GRAY = (150, 150, 150)
CREME = (200, 170, 150)

funcDict = {-1:BLACK, 0:GREEN, 1:BLUE, 2:PURPLE, 3:ORANGE, 4:CYAN, 5:CREME, 6:GRAY, 7:WHITE}

#define window sizes
WINDOW_HEIGHT = 600
WINDOW_WIDTH = 900

#define list of keys and buildings
keys = [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, pygame.K_r]
buildings = [[], [], [], [], [], [], []]

pygame.font.init()
font = pygame.font.SysFont(None, 20)

def main():
    #define empty list of the grid
    grid = [[], []]

    #turn off the drawing of distances
    drawDistance = False

    #turn off game over
    gameOver = False

    #initial turn number
    turn = 1

    #define resources required to build each building
    buildDict = [[5,25,25], [25,5,25], [15,15,25], [25,25,5], [15,25,15], [5,5,5]]

    #define resources required to expand each building
    expandDict = [[0,15,15], [15,0,15], [5,5,15], [15,15,0], [5,15,5]]

    #define resources acquired from each building type
    incomeDict = [[18,0,0], [0,18,0], [14,14,0], [0,0,18], [14,0,14], [-2,-2,-2], [-5,-5,-5]]

    #set up scoring matrices
    scoreList = [[[0,-1,-2,-2,-1], [0,0,0,-1,0], [0,0,0,-1,0], [0,0,0,0,0], [0,0,-1,-1,0]],\
                [[3,2,1,3,2], [3,3,2,3,3], [3,3,3,3,3], [3,2,1,3,2], [3,3,2,3,3]],\
                [-3, -2, -2, -1, -2]]

    #define resources
    resourceNames = ['Community', 'Wealth', 'Production']
    resources = [100,100,100]

    #set current floor to ground floor
    currentLevel = 0

    #define list to identify which building is being expanded
    expandBuilding = [-1, -1]

    #turn off expansion mode
    expansionMode = False

    #define list containing the age of each cell in turn
    cellAge = []

    #no tile is selected when started
    selected = [-1,-1,-1]

    #initial score is 0
    score = 0

    global SCREEN, CLOCK
    pygame.init()

    SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    CLOCK = pygame.time.Clock()

    #draw grid and place courtyards
    grid = drawGrid(currentLevel, drawDistance, grid)
    grid = placeCourtyard(3,3,(5,3), False, grid)
    grid = placeCourtyard(3,3,(15,3), False, grid)

    #calculate income
    income = calculateIncome(buildings, incomeDict, cellAge)

    #draw amount of resources, buildable tiles, and next turn button
    displayResources(resourceNames, resources, income)
    grid = showAdjacentEmpty((5, 6), (0,0), expansionMode, grid)
    nextTurnButton = drawNextTurnButton(score, turn, gameOver)

    while True:
        if gameOver == False:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pressed = pygame.mouse.get_pressed()
                    if pressed[0]:
                        #go to next turn if next turn button is pressed
                        if nextTurnButton.collidepoint(event.pos):
                            turn, resources, cellAge, gameOver = nextTurn(turn, resources, income, cellAge)
                            income = calculateIncome(buildings, incomeDict, cellAge)
                            displayResources(resourceNames, resources, income)
                            score = calculateTotalScore(buildings, grid, scoreList)
                            exportToCSV(grid)
                        else:
                            #if no button is clicked, check if a grid tile is clicked
                            selected = [-1,-1,-1]
                            for z in range(len(grid)):
                                for y in range(len(grid[z])):
                                    for x in range(len(grid[z][y])):
                                        item = grid[z][y][x]
                                        rect = item[0]
                                        item[1] = False

                                        if rect.collidepoint(event.pos) and z == currentLevel:
                                            item[1] = True
                                            selected[0] = currentLevel
                                            selected[1] = y
                                            selected[2] = x
                elif event.type == pygame.KEYDOWN:
                    pressed = pygame.key.get_pressed()

                    #check if a key in the key list is pressed to build buildings
                    for i, key in enumerate(keys):
                        if pressed[key]:
                            resources, cellAge, expandBuilding, selected, expansionMode, income, grid = build(currentLevel, i, resourceNames, resources, cellAge, pressed, expandBuilding, selected, expansionMode, income, buildDict, expandDict, incomeDict, grid)
                    
                    #check all other keys
                    if pressed[pygame.K_e]:
                        #go into expansion mode if a building is selected and the selected building contains less than 4 tiles
                        if selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1 and grid[selected[0]][selected[1]][selected[2]][2] < 5 and buildings[grid[selected[0]][selected[1]][selected[2]][2]][grid[selected[0]][selected[1]][selected[2]][3]] < 4:
                            expansionMode = True
                            grid = showAdjacentEmpty([grid[selected[0]][selected[1]][selected[2]][2]], selected, expansionMode, grid)
                            expandBuilding = [grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3]]
                        else:
                            expansionMode = False
                            grid = showAdjacentEmpty((5, 6), (-1,-1,-1), expansionMode, grid)
                            expandBuilding = [-1, -1]
                    elif pressed[pygame.K_UP]:
                        #go up a floor
                        if currentLevel + 1 != len(grid):
                            currentLevel += 1
                    elif pressed[pygame.K_DOWN]:
                        #go down a floor
                        if currentLevel - 1 != -1:
                            currentLevel -= 1
                    elif pressed[pygame.K_d]:
                        #draw spatial distance if a building is selected
                        if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1 and grid[selected[0]][selected[1]][selected[2]][2] < 5:
                            drawDistance, grid = calculateDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, False)
                            grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 0)
                        else:
                            drawDistance = False
                    elif pressed[pygame.K_c]:
                        #draw connectivity if a building is selected
                        if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1 and grid[selected[0]][selected[1]][selected[2]][2] < 5:
                            drawDistance, grid = calculateDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, True)
                            grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 1)
                        else:
                            drawDistance = False
                    elif pressed[pygame.K_a]:
                        #draw adjacency if a building is selected
                        if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1 and grid[selected[0]][selected[1]][selected[2]][2] < 5:
                            drawDistance, grid, temp = cullDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, 1)
                            grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 2)
                        else:
                            drawDistance = False

            #redraw grid and next turn button   
            drawNextTurnButton(score, turn, gameOver)
            grid = drawGrid(currentLevel, drawDistance, grid)
            pygame.display.update()
        else:
            #show game over screen
            drawNextTurnButton(score, turn, gameOver)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

def calculateTotalScore(buildings, grid, scoreList):
    #check all relation types of all tiles for each tile
    score = 0
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                for i in range(5):
                    if grid[z][y][x][2] == i:
                        for j in range(len(buildings[i])):
                            if grid[z][y][x][3] == j:

                                #calculate distance according to current scope
                                for k in range(3):
                                    grid = setInfluenceFactor(grid[z][y][x][2], grid[z][y][x][3], grid, scoreList, k)
                                    if k == 0:
                                        temp, grid = calculateDistances(grid[z][y][x][2], grid[z][y][x][3], grid, False)
                                    elif k == 1:
                                        temp, grid = calculateDistances(grid[z][y][x][2], grid[z][y][x][3], grid, True)
                                    else:
                                        temp, grid, ratio = cullDistances(grid[z][y][x][2], grid[z][y][x][3], grid, 1)
                                    
                                    for z2 in range(len(grid)):
                                        for y2 in range(len(grid[z2])):
                                            for x2 in range(len(grid[z2][y2])):

                                                #multiply scores by a distance factor (0 distance is full score; 10 distance or more will be outside of the influence range; linear interpolation for values between the two)
                                                if k == 0:
                                                    score += grid[z2][y2][x2][5] * max(0, (10 - max(0, grid[z2][y2][x2][4])))
                                                elif k == 1:
                                                    score += grid[z2][y2][x2][5] * max(0, (10 - max(0, grid[z2][y2][x2][4])))
                                                else:
                                                    score += grid[z2][y2][x2][5] * 10 * ratio

    return int(score)

def setInfluenceFactor(buildingFunction, buildingID, grid, scoreList, scope):
    #setting the influence score of each tile according to the current function and the scoring matrices
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                #the current building is not considered in this scoring
                grid[z][y][x][5] = 0

                if grid[z][y][x][2] > -1 and grid[z][y][x][2] < 5 and (grid[z][y][x][2] != buildingFunction or grid[z][y][x][3] != buildingID):
                    if scope == 2:
                        grid[z][y][x][5] = scoreList[scope][buildingFunction]
                    else:
                        grid[z][y][x][5] = scoreList[scope][buildingFunction][grid[z][y][x][2]]

    return grid

def cullDistances(buildingFunction, buildingID, grid, limit):
    drawDistance, grid = calculateDistances(buildingFunction ,buildingID, grid, False)
    adjacency_count = 0
    built_adjacency_count = 0
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                #only consider tiles with a distance smaller or equal to the limitt distance (1 for adjacency)
                if grid[z][y][x][4] > limit:
                    grid[z][y][x][4] = -1
                else:
                    adjacency_count += 1
                    if grid[z][y][x][2] > -1 and grid[z][y][x][2] < 5:
                        built_adjacency_count += 1

    #calculate the ratio of built adjacent tiles over total number of adjacent tiles
    built_adjacency_ratio = built_adjacency_count / adjacency_count
    return drawDistance, grid, built_adjacency_ratio

def calculateDistances(buildingFunction ,buildingID, grid, connectivity):
    i = 0
    nonConvergence = 0
    while(i <= 3):
        count = 0
        for z in range(len(grid)):
            for y in range(len(grid[z])):
                for x in range(len(grid[z][y])):

                    #set all distances equal to -1 for the first iteration, the distances of inspected room tiles should be equal to 0
                    if i == 0:
                        if grid[z][y][x][2] == buildingFunction and grid[z][y][x][3] == buildingID:
                            grid[z][y][x][4] = 0
                        else:
                            grid[z][y][x][4] = -1
                    else:
                        adjacent = (-1, 1)
                        #check if connectivity is being calculated
                        if connectivity:
                            #check for adjacent tiles whether: 
                            #   it is in the grid
                            #   the adjacent tile has a distance larger than -1
                            #   the current tile has a distance of -1 or that the difference between the distance of the current tile and adjacent tiles is larger than 1
                            #   the current or adjacent tile is either a road or courtyard tile
                            for j in adjacent:
                                if (x + j >= 0 and x + j < len(grid[z][y])) and grid[z][y][x+j][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y][x+j][4] > 1) and (grid[z][y][x+j][2] == 5 or grid[z][y][x+j][2] == 6 or grid[z][y][x][2] == 5 or grid[z][y][x][2] == 6):
                                    grid[z][y][x][4] = grid[z][y][x+j][4] + 1
                                elif (y + j >= 0 and y + j < len(grid[z])) and grid[z][y+j][x][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y+j][x][4] > 1) and (grid[z][y+j][x][2] == 5 or grid[z][y+j][x][2] == 6 or grid[z][y][x][2] == 5 or grid[z][y][x][2] == 6):
                                    grid[z][y][x][4] = grid[z][y+j][x][4] + 1
                                
                                #if there is still an unreached tile left, set i to 1 and add 1 to the amount of unreached tiles count
                                if grid[z][y][x][4] == -1 and (grid[z][y][x][2] == 5 or grid[z][y][x][2] == 6):
                                    i = 1
                                    count += 1
                        else:
                            #check for adjacent tiles whether: 
                            #   it is in the grid
                            #   the adjacent tile has a distance larger than -1
                            #   the current tile has a distance of -1 or that the difference between the distance of the current tile and adjacent tiles is larger than 1
                            for j in adjacent:
                                if (x + j >= 0 and x + j < len(grid[z][y])) and grid[z][y][x+j][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y][x+j][4] > 1):
                                    grid[z][y][x][4] = grid[z][y][x+j][4] + 1
                                elif (y + j >= 0 and y + j < len(grid[z])) and grid[z][y+j][x][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y+j][x][4] > 1):
                                    grid[z][y][x][4] = grid[z][y+j][x][4] + 1
                                elif (z + j >= 0 and z + j < len(grid)) and grid[z+j][y][x][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z+j][y][x][4] > 1):
                                    grid[z][y][x][4] = grid[z+j][y][x][4] + 1
                                
                                #if there is still an unreached tile left, set i to 1 (all tiles are reachable so unreached tiles are not counted)
                                if grid[z][y][x][4] == -1:
                                    i = 1
        i += 1
        #add 1 to the iteration count and check if 100 iterations are done
        nonConvergence += 1
        if nonConvergence == 100:
            print(str(int(count/2)) + " first floor tiles could not be reaced within 100 iterations")
            break
    return True, grid                      

def nextTurn(turn, resources, income, cellAge):
    gameOver = False
    turn += 1

    #the game is over if any of the resources is below zero
    for i in range(len(resources)):
        resources[i] += income[i]
        if resources[i] < 0:
            gameOver = True
    
    #each cell is one turn older
    for i in range(len(cellAge)):
        cellAge[i][1] += 1
    
    return turn, resources, cellAge, gameOver

def build(currentLevel, funcID, resourceNames, resources, cellAge, pressed, expandBuilding, selected, expansionMode, income, buildDict, expandDict, incomeDict, grid):
    for row in grid[currentLevel]:
        for item in row:
            active = item[1]
            #check whether a tile is selected, and the selected tile is buildable
            if active == True and item[2] == 7:
                #check whether a building is not expanded
                if expandBuilding == [-1, -1]:
                    for i in range(len(buildDict[funcID])):
                        #don't build if one of the required resources is larger than the current resources
                        if buildDict[funcID][i] > resources[i]:
                            return resources, cellAge, expandBuilding, selected, expansionMode, income, grid
                    
                    item[2] = funcID
                    item[3] = len(buildings[funcID])

                    #append the length of the building in the building list
                    buildings[funcID].append(1)

                    #recalculate resources
                    for i in range(len(resources)):
                        resources[i] = resources[i] - buildDict[funcID][i]

                    #redraw buildable tiles if a road is built, else append new tile age to the cell age list
                    if pressed[pygame.K_r]:
                        grid = showAdjacentEmpty((5, 6), (-1,-1,-1), expansionMode, grid)
                    else:
                        cellAge.append([funcID, 0])

                else:
                    for i in range(len(expandDict[funcID])):
                        #don't build if one of the required resources is larger than the current resources
                        if expandDict[funcID][i] > resources[i]:
                            return resources, cellAge, expandBuilding, selected, expansionMode, income, grid
                    

                    if expandBuilding[0] == funcID:
                        item[2] = funcID
                        item[3] = expandBuilding[1]

                        #increase size of existing building by 1
                        buildings[expandBuilding[0]][expandBuilding[1]] += 1

                        #append new tile age to the cell age list
                        cellAge.append([funcID, 0])

                        #recalculate resources
                        for i in range(len(resources)):
                            resources[i] = resources[i] - expandDict[funcID][i]

                        #leave expansion mode if building is 4 blocks large
                        if buildings[expandBuilding[0]][expandBuilding[1]] < 4:
                            grid = showAdjacentEmpty([grid[selected[0]][selected[1]][selected[2]][2]], selected, expansionMode, grid)
                        else:
                            expansionMode = False
                            grid = showAdjacentEmpty((5, 6), (-1,-1,-1), expansionMode, grid)
                            expandBuilding = [-1, -1]
                
                #deselect tile after having something build
                item[1] = False
                selected = [-1,-1,-1]

                #recalculate income
                income = calculateIncome(buildings, incomeDict, cellAge)
                displayResources(resourceNames, resources, income)

    return resources, cellAge, expandBuilding, selected, expansionMode, income, grid

def drawNextTurnButton(score, turn, gameOver):
    rect = pygame.Rect(WINDOW_WIDTH - 200, WINDOW_HEIGHT-100, 200, 100)
    pygame.draw.rect(SCREEN, BLACK, rect)

    #display score
    text = font.render('score: ' + str(score), True, WHITE)
    SCREEN.blit(text, (WINDOW_WIDTH - 110, WINDOW_HEIGHT-40))

    #display turn number
    text = font.render('turn ' + str(turn), True, WHITE)
    SCREEN.blit(text, (WINDOW_WIDTH - 80, WINDOW_HEIGHT-90))

    if gameOver == True:
        text = font.render('GAME OVER', True, WHITE)
        SCREEN.blit(text, (WINDOW_WIDTH - 100, WINDOW_HEIGHT-70))
    else:
        text = font.render('NEXT TURN', True, WHITE)
        SCREEN.blit(text, (WINDOW_WIDTH - 100, WINDOW_HEIGHT-70))
        nextTurnButton = pygame.Rect(WINDOW_WIDTH - 110, WINDOW_HEIGHT-75, 95, 30)
        pygame.draw.rect(SCREEN, WHITE, nextTurnButton, 1)
        return nextTurnButton 

def calculateIncome(buildings, incomeDict, cellAge):
    income = [0,0,0]

    #each successive block in a single building yields less resources. This follows the harmonic series
    for i in range(len(buildings)):
        for j in range(len(buildings[i])):
            factor = 0
            for number in range(1, buildings[i][j] + 1):
                factor += 1/number
            for k in range(len(income)):
                income[k] += int(factor * incomeDict[i][k])
    
    for i in range(len(cellAge)):
        for j in range(len(income)):
            #the income of each tile is decreased by (age/15)^2 times the income. This number is capped at a penalty of -20
            income[j] += max(int(min(incomeDict[cellAge[i][0]][j] * -1, 0) * math.pow(cellAge[i][1]/15, 2)), -20)
    return income 

def displayResources(resourceNames, resources,income):
    SCREEN.fill(BLACK)  
    for i in range(len(resources)):
        text = font.render(resourceNames[i] + ': ' + str(resources[i]) + ' (' + str(income[i]) + ')', True, WHITE)
        SCREEN.blit(text, (25, WINDOW_HEIGHT-(len(resources)-i)*30))

def drawGrid(level, drawDistance, grid):
    blockSize = 25

    #create tiles
    for z in range(len(grid)):
        if len(grid[z]) == 0:
            for y in range(0, WINDOW_HEIGHT-100, blockSize):
                row = []
                for x in range(0, WINDOW_WIDTH, blockSize):
                    rect = pygame.Rect(x, y, blockSize, blockSize)
                    #each tile has data on:
                        #   its visual rectangle
                        #   whether it is selected
                        #   its room function
                        #   its id number for its room function
                        #   its distance
                        #   its influence factor
                    row.append([rect, False, -1, 0, -1, 0])
                grid[z].append(row)

    #draw grid
    for row in grid[level]:
        for item in row:
            rect = item[0]
            buildingID = item[3]

            #if distances should not be drawn, each tile will have its colour according to its function or to whether it is selected
            if drawDistance == False:
                color = funcDict[item[2]]
                pygame.draw.rect(SCREEN, color, rect)
                pygame.draw.rect(SCREEN, WHITE, rect, 1)
                if item[1] == True:
                    pygame.draw.rect(SCREEN, RED, rect)
                text = font.render(str(buildingID), True, WHITE)

            else:
                #set negative distances the maximum distance
                maxDistance = len(grid) + len(grid[0]) + len(row)
                if item[4] == -1:
                    item[4] = maxDistance

                colorIndex = max(0, int((maxDistance - item[4]*(maxDistance/10))/maxDistance * 125))

                #negative scores are drawn red, positive scores are drawn green, neutral scores are drawn cyan
                if item[5] < 0:
                    color = (min(255, -item[5] * colorIndex), 0, 0.1*colorIndex)
                elif item[5] > 0:
                    color = (0, min(255, item[5] * colorIndex), 0.3*colorIndex)
                else:
                    color = (0, colorIndex, colorIndex)

                pygame.draw.rect(SCREEN, color, rect)
                pygame.draw.rect(SCREEN, WHITE, rect, 1)
                if item[1] == True:
                    pygame.draw.rect(SCREEN, RED, rect)
                text = font.render(str(buildingID), True, WHITE)
                text = font.render(str(item[4]), True, WHITE)

            SCREEN.blit(text, (rect[0], rect[1]))
    return grid

def placeCourtyard(x, y, p, hasPath, grid):
    #place courtyard of specified size on a specified location
    for row in range(p[1],p[1]+x):
        for item in range(p[0],p[0]+y):
            grid[0][row][item][2] = 6
    
    #connect courtyard with a road to the edge of the grid
    if hasPath == False:
        if p[0] < p[1]:
            for item in range(p[0]):
                grid[0][p[1] + math.floor(y/2)][item][2] = 5
        else:
            for row in range(p[1]):
                grid[0][row][p[0] + math.floor(x/2)][2] = 5

    #add the courtyard to the building list
    buildings[6].append(1)
    return grid

def showAdjacentEmpty(functions, selected, expansion, grid):
    #reset buildable tiles
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                if grid[z][y][x][2] == 7:
                    grid[z][y][x][2] = -1

    for function in functions:
        if expansion == False:
            for z in range(len(grid)):
                for y in range(len(grid[z])):
                    for x in range(len(grid[z][y])):
                        #define tiles buildable if they are next to the following function
                        if grid[z][y][x][2] == function:
                            adjacent = (-1,1)
                            for i in adjacent:
                                #check whether adjacent tile is in grid and whether it is empty
                                if (y+i != -1) and (y+i != len(grid[z])) and grid[z][y+i][x][2] == -1:
                                    grid[z][y+i][x][2] = 7
                                if (x+i != -1) and (x+i != len(grid[z][y])) and grid[z][y][x+i][2] == -1:
                                    grid[z][y][x+i][2] = 7
        elif grid[selected[0]][selected[1]][selected[2]][2] == function:
            for z in range(len(grid)):
                for y in range(len(grid[z])):
                    for x in range(len(grid[z][y])):
                        #check whether current tile belongs to the building that is being expanded
                        if (grid[selected[0]][selected[1]][selected[2]][2] == grid[z][y][x][2]) and (grid[selected[0]][selected[1]][selected[2]][3] == grid[z][y][x][3]):
                            adjacent = (-1,1)
                            for i in adjacent:
                                #check whether adjacent tile is in grid and whether it is empty
                                if (z+i != -1) and (z+i != len(grid)) and grid[z+i][y][x][2] == -1:
                                    grid[z+i][y][x][2] = 7
                                if (y+i != -1) and (y+i != len(grid[z])) and grid[z][y+i][x][2] == -1:
                                    #if expandin on higher floors: check whether tile below is built or whether two tiles away a building is present (this allows for skyways)
                                    if z > 0:
                                        if (grid[z-1][y+i][x][2] > -1 and grid[z-1][y+i][x][2] < 5) or (grid[z][y+2*i][x][2] > -1 and grid[z][y+2*i][x][2] < 5):
                                            grid[z][y+i][x][2] = 7
                                    else:
                                        grid[z][y+i][x][2] = 7
                                if (x+i != -1) and (x+i != len(grid[z][y])) and grid[z][y][x+i][2] == -1:
                                    if z > 0:
                                        if (grid[z-1][y][x+i][2] > -1 and grid[z-1][y][x+i][2] < 5) or (grid[z][y][x+2*i][2] > -1 and grid[z][y][x+2*i][2] < 5):
                                                grid[z][y][x+i][2] = 7
                                    else:
                                        grid[z][y][x+i][2] = 7
    return grid

def exportToCSV(grid):
    #export results as a .csv file
    abs_path = Path(os.path.realpath(__file__))
    parent_path = str(abs_path.parent)

    for z in range(len(grid)):
        functionsGrid = []
        for y in range(len(grid[z])):
            functions = []
            ids = []
            for x in range(len(grid[z][y])):
                functions.append(grid[z][y][x][2])
            functionsGrid.append(functions) 
        f = open(parent_path + '/functionsLevel' + str(z) + '.csv', 'w')
        write = csv.writer(f)
        write.writerows(functionsGrid)

main()