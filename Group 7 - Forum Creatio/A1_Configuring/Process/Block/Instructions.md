# EARTHY4.0G7 - Block scale game
### Written by Kaan Akbaba
### Last revision: 02/11/2021

## SETTING UP THE GAME
First you need to install pygame in vscode by entering the following command into the terminal:  
- py -m pip install -U pygame --user
  
If that doesn't work, try installing the latest python version from https://www.python.org/downloads/ before installing pygame; this solved issues for me, at least. Let me know if any issues persist for you.  

## GAME CONTROLS
First you need to select a tile; you will see it turn red, it is now selected. At selected white tiles, buildings or roads can be built. Buildings can only be placed next to road or courtyard tiles. Here is a summary of building costs:  
- House (1):
    - 5 community
    - 25 wealth
    - 25 production
- Shop (2):
    - 25 community
    - 5 wealth
    - 25 production
- Restaurant (3):
    - 15 community
    - 15 wealth
    - 25 production
- Workshop (4):
    - 25 community
    - 25 wealth
    - 5 production
- Creative workshop (5):
    - 15 community
    - 25 wealth
    - 15 production
- Road (r):
    - 5 community
    - 5 wealth
    - 5 production
  
The keys to build each building are mentioned above between brackets. Note that buildings can only be built if a white tile is selected and if there are sufficient resources available! Clicking on next turn will either increase or decrease the amount of available resources, depending on the income.  
Below are listed the incomes of each building type:  
- House (1):
    - 18 community
    - 0 wealth
    - 0 production
- Shop (2):
    - 0 community
    - 18 wealth
    - 0 production
- Restaurant (3):
    - 14 community
    - 14 wealth
    - 0 production
- Workshop (4):
    - 0 community
    - 0 wealth
    - 18 production
- Creative workshop (5):
    - 14 community
    - 0 wealth
    - 14 production
- Road (r):
    - -2 community
    - -2 wealth
    - -2 production
  
A selected building can be expanded more cheaply, but be aware that expanding buildings give less resources than making new ones. Pressing the 'e' key when a building is selected will enter expansion mode (you can leave expansion mode by pressing 'e' again).  
When in eexpansion mode all tiles that are adjacent to the building are turned white. Expansion mode is automatically disabled for buildings that are 4 cells large. Below are the expansion costs of the 3 building types:  
- House (1):
    - 0 community
    - 15 wealth
    - 15 production
- Shop (2):
    - 15 community
    - 0 wealth
    - 15 production
- Restaurant (3):
    - 5 community
    - 5 wealth
    - 15 production
- Workshop (4):
    - 15 community
    - 15 wealth
    - 0 production
- Creative workshop (5):
    - 5 community
    - 15 wealth
    - 5 production
  
Be aware that buildings can also be expanded upwards! By pressing the up and down key, the player can alternate between levels. Note that a skyway can be built between two two-storey buildings while expanding a building!  
Finally, be aware that buildings will get lower income over time, and after a number of turns, cells may even cost more than they are worth! So, it is important to keep expanding!  
The game ends when one of the resources has a value below zero.  
  
At every moment in the game relations between buildings can be inspected. The relations considered are:  
- Spatial distance (d)
- Connectivity (c)
- Adjacency (a)
  
Between brackets is indicated which key to press to inspect this relation. Be sure to have a building selected before inspecting one of these relations!  
The colours while inspecting the interfunctional relations indicate whether the influence is positive (green), negative (red), or neutral (cyan). The dimmer the colour, the lower the influence.  
When pressing the next turn button all relations for all buildings are calculated, and a total score for the entire configuration is returned.  

## VISUALISING RESULTS
With the 'Render bazaar.gh' script, the game can be visualised in 3D space. It should work when starting up the grasshopper script. If it does not, be sure to link the 'read file' components at the start of the script to the .csv files in the folder. The upper read file component should be connected to 'functionsLevel0.csv', while the bottom one should be connected to 'functionsLevel1.csv'. Please let me know if issues persist.  
  
Have fun and let me know what you think!  