# Made by Kaan Akbaba
# Last revised: 02/11/2021

import pygame
import sys
import math
import csv
import copy
import time
import os
from pathlib import Path

from pygame import draw


#define colours
BLACK = (0, 0, 0)
WHITE = (200, 200, 200)
RED = (255, 0, 0)
DARK_RED = (120,0,0)
GREEN = (0, 255, 0)
DARK_GREEN = (0,120,0)
BLUE = (0, 100, 255)
PURPLE = (230, 0, 230)
ORANGE = (255,100,0)
CYAN = (0, 230, 230)
GRAY = (150, 150, 150)
CREME = (200, 170, 150)
KHAKI = (195, 176, 145)
YELLOW = (230,230,0)

funcColourDict = {-1:BLACK, 0:GREEN, 1:BLUE, 2:PURPLE, 3:ORANGE, 4:CYAN, 5:CREME, 6:GRAY, 7:WHITE}

#define window sizes
WINDOW_HEIGHT = 752
WINDOW_WIDTH = 1200

#define list of keys and rooms
wallKeys = [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4]
functions = [("Entrance", "Corridor", "Staircase", "Living Room", "Kitchen", "Bathroom H", "Bathroom V", "Bedroom", "Dining room", "Storage H", "Storage V", "Courtyard", "Terrace H", "Terrace V", "Study room H", "Study room V", "Toilet"),\
                ("Entrance", "Corridor", "Staircase", "Shop Area 2x2", "Shop Area H", "Shop Area V", "Shop Area 1x1", "Canteen H", "Canteen V", "Storage 2x2", "Storage H", "Storage V", "Storage 1x1", "Toilet"),\
                ("Entrance", "Corridor", "Staircase", "Dining area 2x2", "Dining area H", "Dining area V", "Dining area 1x1", "Kitchen", "Storage H", "Storage V", "Terrace", "Toilet"),\
                ("Entrance", "Corridor", "Staircase", "Workplace 2x2", "Workplace H", "Workplace V", "Workplace 1x1", "Canteen H", "Canteen V", "Storage H", "Storage V", "Toilet"),\
                ("Entrance", "Corridor", "Staircase", "Workplace 2x2", "Workplace H", "Workplace V", "Workplace 1x1", "Learning area 2x2", "Learning area H", "Learning area V", "Learning area 1x1", "Canteen H", "Canteen V", "Storage H", "Storage V", "Toilet")]

pygame.font.init()
font = pygame.font.SysFont(None, 20)

def main():
    #define empty list of walls and grid
    walls = []
    grid = [[], []]

    #turn off the drawing of distances
    drawDistance = False

    #set up scoring matrices (can be avoided if classes are used)
    scoreList = [\
                    [\
                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0],\
                        [0,0,0,0,-1,0,0,0,-1,0,0,0,-2,-2,0,0,0],\
                        [0,0,0,0,-1,0,0,0,-1,0,0,0,-2,-2,0,0,0],\
                        [0,0,0,-1,-2,0,0,0,-1,0,0,-1,-3,-3,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-2,0,0,-1,-2,0,0,-1,-3,-3,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,0,0,0]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,0,0,0,-1,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,0,0,0,-1,0]],\
                        
                        [[0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,0,0,0],\
                        [0,0,0,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,-1,-1,-1,-1,0,0,0,0,0,0,0,0,0]],\
                    ],\
                    [\
                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [2,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [1,0,0,3,4,4,4,4,3,4,4,3,3,3,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [2,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [2,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4],\
                        [0,0,0,2,3,4,4,4,2,4,4,2,2,2,4,4,4]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,4,4,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,4,4,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4,4,4]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [4,0,0,4,4,4,4,4,4,4,4,4],\
                        [4,0,0,4,4,4,4,4,4,4,4,4],\
                        [4,0,0,4,4,4,4,4,4,4,4,4],\
                        [4,0,0,4,4,4,4,4,4,4,4,4],\
                        [1,0,0,1,1,1,1,4,4,4,3,4],\
                        [0,0,0,0,0,0,0,3,4,4,2,4],\
                        [0,0,0,0,0,0,0,3,4,4,2,4],\
                        [2,0,0,2,2,2,2,4,4,4,4,4],\
                        [0,0,0,0,0,0,0,3,4,4,2,4]],\
                        
                        [[0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0],\
                        [3,0,0,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4],\
                        [0,0,0,1,1,1,1,3,3,4,4,4]],\

                        [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [3,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,3,3,3,3,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,3,3,3,3,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,3,3,3,3,4,4,4,4,4,4,4,4,4],\
                        [2,0,0,3,3,3,3,4,4,4,4,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,3,3,3,3,4,4,4,4,4],\
                        [1,0,0,2,2,2,2,3,3,3,3,4,4,4,4,4],\
                        [0,0,0,1,1,1,1,2,2,2,2,3,3,4,4,4],\
                        [0,0,0,1,1,1,1,2,2,2,2,3,3,4,4,4],\
                        [0,0,0,1,1,1,1,2,2,2,2,3,3,4,4,4]],\
                    ],\
                    [\
                        [0,0,0,-9,-6,-3,-3,-2,-9,0,0,-3,-12,-12,-6,-6,0],\
                        [0,0,0,-9,-9,-9,-9,-6,-6,0,0,0,0,0],\
                        [0,0,0,-9,-9,-9,-9,-6,0,0,-12,0],\
                        [0,0,0,-3,-3,-3,-3,-6,-6,0,0,0],\
                        [0,0,0,-3,-3,-3,-3,-3,-3,-3,-3,-6,-6,0,0,0],\
                    ]\
                ]

    #set first floor as current floor
    currentLevel = 0

    #no building type is selected when started
    currentType = -1

    #no room type is selected when started
    selectedFunction = -1

    #no tile is selected when started
    selected = [-1,-1,-1]

    #initial score is 0
    score = 0

    global SCREEN, CLOCK
    pygame.init()

    SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    CLOCK = pygame.time.Clock()

    #define lists for building types
    buildingTypes = ("Residential", "Shop", "Restaurant", "Workshop", "Creative workshop")

    #define lists for room colours and sizes
    functionColours = ((RED, GRAY, KHAKI, GREEN, ORANGE, BLUE, BLUE, YELLOW, CREME, DARK_RED, DARK_RED, DARK_GREEN, CYAN, CYAN, PURPLE, PURPLE, WHITE),\
                       (RED, GRAY, KHAKI, GREEN, GREEN, GREEN, GREEN, ORANGE, ORANGE, DARK_RED, DARK_RED, DARK_RED, DARK_RED, WHITE),\
                       (RED, GRAY, KHAKI, GREEN, GREEN, GREEN, GREEN, ORANGE, DARK_RED, DARK_RED, CYAN, WHITE),\
                       (RED, GRAY, KHAKI, GREEN, GREEN, GREEN, GREEN, ORANGE, ORANGE, DARK_RED, DARK_RED, WHITE),\
                       (RED, GRAY, KHAKI, GREEN, GREEN, GREEN, GREEN, CYAN, CYAN, CYAN, CYAN, ORANGE, ORANGE, DARK_RED, DARK_RED, WHITE))
    functionSizes = [((1,1), (1,1), (2,2), (2,2), (2,2), (2,1), (1,2), (2,2), (2,2), (2,1), (1,2), (4,4), (2,1), (1,2), (2,1), (1,2), (1,1)),\
                     ((1,1), (1,1), (2,2), (2,2), (2,1), (1,2), (1,1), (2,1), (1,2), (2,2), (2,1), (1,2), (1,1), (1,1)),\
                     ((1,1), (1,1), (2,2), (2,2), (2,1), (1,2), (1,1), (2,2), (2,1), (1,2), (1,1), (1,1)),\
                     ((1,1), (1,1), (2,2), (2,2), (2,1), (1,2), (1,1), (2,1), (1,2), (2,1), (1,2), (1,1)),\
                     ((1,1), (1,1), (2,2), (2,2), (2,1), (1,2), (1,1), (2,2), (2,1), (1,2), (1,1), (2,1), (1,2), (2,1), (1,2), (1,1))]
    
    #set up grid based on the results from the results of the block configuration game
    grid = drawGrid(currentLevel, drawDistance, grid, walls, currentType, functionColours)
    grid = importFunctionFromCSV(grid, 2, 5)

    #define lists for amount of rooms allowed per type and amount of rooms built per type
    voxelCount = len(grid) * len(grid[0]) * len(grid[0][0])
    functionAllowedCount = [[5, voxelCount, 1, 2, 1, 2, 2, 4, 2, 2, 2, 1, 5, 5, 3, 3, 3],\
                            [5, voxelCount, 1, 5, 5, 5, 5, 3, 3, 4, 4, 4, 4, 6],\
                            [5, voxelCount, 1, 5, 5, 5, 5, 3, 4, 4, 10, 6],\
                            [5, voxelCount, 1, 3, 3, 3, 3, 5, 5, 5, 5, 6],\
                            [5, voxelCount, 1, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 6]]
    functionBuiltCount = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],\
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    #draw function selection buttons
    functionButton = drawFunctionButton(currentType, buildingTypes, functions, functionAllowedCount, selectedFunction)
    text = font.render("Score: " + str(score), True, WHITE)
    SCREEN.blit(text, (WINDOW_WIDTH - 120, WINDOW_HEIGHT - 20))

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pressed = pygame.mouse.get_pressed()
                if pressed[0]:
                    for i in range(len(functionButton)):
                        if functionButton[i].collidepoint(event.pos):
                            #set building type if none is selected yet, else select room type
                            if currentType == -1:
                                currentType = i
                                functionButton = drawFunctionButton(currentType, buildingTypes, functions, functionAllowedCount, selectedFunction)
                                text = font.render("Score: " + str(score), True, WHITE)
                                SCREEN.blit(text, (WINDOW_WIDTH - 120, WINDOW_HEIGHT - 20))
                                break
                            else:
                                selectedFunction = i
                                functionButton = drawFunctionButton(currentType, buildingTypes, functions, functionAllowedCount, selectedFunction)
                                text = font.render("Score: " + str(score), True, WHITE)
                                SCREEN.blit(text, (WINDOW_WIDTH - 120, WINDOW_HEIGHT - 20))
                                break
                    
                    #if no button is clicked, check if a grid tile is clicked
                    selected = [-1,-1,-1]
                    for z in range(len(grid)):
                        for y in range(len(grid[z])):
                            for x in range(len(grid[z][y])):
                                item = grid[z][y][x]
                                rect = item[0]
                                item[1] = False

                                if rect.collidepoint(event.pos) and z == currentLevel:
                                    item[1] = True
                                    selected[0] = currentLevel
                                    selected[1] = y
                                    selected[2] = x
            elif event.type == pygame.KEYDOWN:
                pressed = pygame.key.get_pressed()

                #check if any of the keys in the key list are pressed
                for i in range(len(wallKeys)):
                    if pressed[wallKeys[i]]:
                        walls = adjustWalls(walls, selected, i)

                #check if all other keys are pressed
                if pressed[pygame.K_b]:
                    #if a tile is selected, try to place a room and redraw the buttons
                    if selectedFunction > -1 and functionAllowedCount[currentType][selectedFunction] > 0:
                        grid, walls, functionAllowedCount, functionBuiltCount = placeFunction(currentType, functionSizes, (selected[2], selected[1], selected[0]), grid, selectedFunction, functionAllowedCount, functionBuiltCount, walls)
                        functionButton = drawFunctionButton(currentType, buildingTypes, functions, functionAllowedCount, selectedFunction)
                        text = font.render("Score: " + str(score), True, WHITE)
                        SCREEN.blit(text, (WINDOW_WIDTH - 120, WINDOW_HEIGHT - 20))

                elif pressed[pygame.K_UP]:
                    #move up a floor
                    if currentLevel + 1 != len(grid):
                        currentLevel += 1

                elif pressed[pygame.K_DOWN]:
                    #move down a floor
                    if currentLevel - 1 != -1:
                        currentLevel -= 1

                elif pressed[pygame.K_d]:
                    #draw spatial distance map if a room is selected
                    if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1:
                        drawDistance, grid = calculateDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, False)
                        grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 0, currentType)
                    else:
                        drawDistance = False

                elif pressed[pygame.K_c]:
                    #draw connectivity map if a room is selected
                    if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1:
                        drawDistance, grid = calculateDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, True, walls)
                        grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 1, currentType)
                    else:
                        drawDistance = False

                elif pressed[pygame.K_a]:
                    #draw adjacency map if a room is selected
                    if drawDistance == False and selected != [-1,-1,-1] and grid[selected[0]][selected[1]][selected[2]][2] > -1:
                        drawDistance, grid, temp = cullDistances(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, 1, currentType)
                        grid = setInfluenceFactor(grid[selected[0]][selected[1]][selected[2]][2], grid[selected[0]][selected[1]][selected[2]][3], grid, scoreList, 2, currentType)
                    else:
                        drawDistance = False

                elif pressed[pygame.K_RETURN]:
                    #calculate score of current configuration
                    if currentType != -1:
                        score = calculateTotalScore(grid, scoreList, currentType, functionBuiltCount, walls)
                        functionButton = drawFunctionButton(currentType, buildingTypes, functions, functionAllowedCount, selectedFunction)
                        text = font.render("Score: " + str(score), True, WHITE)
                        SCREEN.blit(text, (WINDOW_WIDTH - 120, WINDOW_HEIGHT - 20))

        #redraw grid
        grid = drawGrid(currentLevel, drawDistance, grid, walls, currentType, functionColours)
        pygame.display.update()

def adjustWalls(walls, selected, wallID):
    #toggle placing or removing a wall: 1=top edge of selection; 2=right edge of selection; 3=bottom edge of selection; 4=left edge of selection
    if wallID == 0:
        if walls[selected[0]][selected[1]][selected[2]][0][0][1] == 0:
            walls[selected[0]][selected[1]][selected[2]][0][0][1] = 1
        else:
            walls[selected[0]][selected[1]][selected[2]][0][0][1] = 0
    elif wallID == 1:
        if walls[selected[0]][selected[1]][selected[2] + 1][0][1][1] == 0:
            walls[selected[0]][selected[1]][selected[2] + 1][0][1][1] = 1
        else:
            walls[selected[0]][selected[1]][selected[2] + 1][0][1][1] = 0
    elif wallID == 2:
        if walls[selected[0]][selected[1] + 1][selected[2]][0][0][1] == 0:
            walls[selected[0]][selected[1] + 1][selected[2]][0][0][1] = 1
        else:
            walls[selected[0]][selected[1] + 1][selected[2]][0][0][1] = 0
    elif wallID == 3:
        if walls[selected[0]][selected[1]][selected[2]][0][1][1] == 0:
            walls[selected[0]][selected[1]][selected[2]][0][1][1] = 1
        else:
            walls[selected[0]][selected[1]][selected[2]][0][1][1] = 0
    return walls

def drawFunctionButton(currentType, buildingTypes, functions, count, selected):
    #draw a black box first to avoid fuzzy overlaps
    pygame.draw.rect(SCREEN, BLACK, pygame.Rect(WINDOW_WIDTH - 200, 0, 200, WINDOW_HEIGHT))

    #draw buttons
    functionButtons = []
    if currentType != -1:
        for i in range(len(functions[currentType])):
            functionButton = pygame.Rect(WINDOW_WIDTH - 180, 30*(i+1), 160, 30)
            functionButtons.append(functionButton)

            #check if a room type is selected
            if selected == i:
                pygame.draw.rect(SCREEN, WHITE, functionButton)
                text = font.render(str(functions[currentType][i]) + ' ' + str(count[currentType][i]) + 'x', True, BLACK)
            else:
                pygame.draw.rect(SCREEN, BLACK, functionButton)
                text = font.render(str(functions[currentType][i]) + ' ' + str(count[currentType][i]) + 'x', True, WHITE)
            pygame.draw.rect(SCREEN, WHITE, functionButton, 1)

            SCREEN.blit(text, (functionButton[0] + 10, functionButton[1] + 6))
    else:
        for i in range(len(buildingTypes)):
            functionButton = pygame.Rect(WINDOW_WIDTH - 180, 30*(i+1), 160, 30)
            functionButtons.append(functionButton)
            pygame.draw.rect(SCREEN, BLACK, functionButton)
            pygame.draw.rect(SCREEN, WHITE, functionButton, 1)
            text = font.render(str(buildingTypes[i]), True, WHITE)

            SCREEN.blit(text, (functionButton[0] + 10, functionButton[1] + 6))
    return functionButtons

def calculateTotalScore(grid, scoreList, currentType, functionCount, walls):
    #check all relation types of all tiles for each tile
    score = 0
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                for i in range(len(functionCount[currentType])):
                    if grid[z][y][x][2] == i:
                        for j in range(functionCount[currentType][i]):
                            if grid[z][y][x][3] == j:

                                #calculate distance according to current scope
                                for scope in range(3):
                                    if scope == 0:
                                        temp, grid = calculateDistances(grid[z][y][x][2], grid[z][y][x][3], grid, False)
                                    elif scope == 1:
                                        temp, grid = calculateDistances(grid[z][y][x][2], grid[z][y][x][3], grid, True, walls)
                                    else:
                                        temp, grid, ratio = cullDistances(grid[z][y][x][2], grid[z][y][x][3], grid, 1, currentType)

                                    grid = setInfluenceFactor(grid[z][y][x][2], grid[z][y][x][3], grid, scoreList, scope, currentType)

                                    for z2 in range(len(grid)):
                                        for y2 in range(len(grid[z2])):
                                            for x2 in range(len(grid[z2][y2])):

                                                #multiply scores by a distance factor (0 distance is full score; 10 distance or more will be outside of the influence range; linear interpolation for values between the two)
                                                if scope == 0:
                                                    score += grid[z2][y2][x2][5] * max(0, (10 - grid[z2][y2][x2][4]))
                                                elif scope == 1:
                                                    score += grid[z2][y2][x2][5] * max(0, (10 - grid[z2][y2][x2][4]))
                                                else:
                                                    score += grid[z2][y2][x2][5] * 0.05 * ratio
    return int(score / 10)

def setInfluenceFactor(roomFunction, roomID, grid, scoreList, scope, currentType):
    #setting the influence score of each tile according to the current function and the scoring matrices
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                #the current room is not considered in this scoring
                grid[z][y][x][5] = 0

                if (grid[z][y][x][2] != roomFunction or grid[z][y][x][3] != roomID):
                    if scope == 2 and (grid[z][y][x][2] == -1 or (grid[z][y][x][2] != 0 and ("Terrace" not in functions[currentType][grid[z][y][x][2]]))):
                        grid[z][y][x][5] = scoreList[scope][currentType][roomFunction]
                    elif scope != 2 and grid[z][y][x][2] > -1:
                        grid[z][y][x][5] = scoreList[scope][currentType][roomFunction][grid[z][y][x][2]]
    return grid

def cullDistances(buildingFunction, buildingID, grid, limit, currentType):
    drawDistance, grid = calculateDistances(buildingFunction ,buildingID, grid, False)
    adjacency_count = 0
    built_adjacency_count = 0
    maxDistance = len(grid) * len(grid[0]) * len(grid[0][0])

    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                #only consider tiles with a distance smaller or equal to the limitt distance (1 for adjacency)
                if grid[z][y][x][4] > limit:
                    grid[z][y][x][4] = maxDistance

                else:
                    adjacency_count += 1
                    #don't consider terraces or entraces as tiles with a building on them (empty tiles are considered built tiles, as we don't know anything about other buildings)
                    if grid[z][y][x][2] == -1 or (grid[z][y][x][2] != 0 and ("Terrace" not in functions[currentType][grid[z][y][x][2]])):
                        built_adjacency_count += 1

    #calculate the ratio of built adjacent tiles over total number of adjacent tiles
    built_adjacency_ratio = built_adjacency_count / adjacency_count
    return drawDistance, grid, built_adjacency_ratio

def calculateDistances(buildingFunction ,buildingID, grid, connectivity, walls=0):
    i = 0
    iterationCount = 0


    while(i <= 3):
        count = 0
        for z in range(len(grid)):
            for y in range(len(grid[z])):
                for x in range(len(grid[z][y])):

                    #set all distances equal to -1 for the first iteration, the distances of inspected room tiles should be equal to 0
                    if i == 0:
                        if grid[z][y][x][2] == buildingFunction and grid[z][y][x][3] == buildingID:
                            grid[z][y][x][4] = 0
                        else:
                            grid[z][y][x][4] = -1
                    else:
                        adjacent = (-1, 1)
                        #check if connectivity is being calculated
                        if connectivity:
                            #check for adjacent tiles whether: 
                            #   it is in the grid
                            #   the adjacent tile has a distance larger than -1
                            #   there is no wall between the current tile and the adjacent tile
                            #   the first floor is checked and, if not, that the tile is not empty (to prevent floating)
                            #   the current tile has a distance of -1 or that the difference between the distance of the current tile and adjacent tiles is larger than 1
                            for j in adjacent:
                                if (x + j >= 0 and x + j < len(grid[z][y])) and (grid[z][y][x+j][4] > -1 and walls[z][y][x+max(0,j)][0][1][1] == 0 and (z == 0 or grid[z][y][x][2] != -1)) and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y][x+j][4] > 1):
                                    grid[z][y][x][4] = grid[z][y][x+j][4] + 1
                                elif (y + j >= 0 and y + j < len(grid[z])) and (grid[z][y+j][x][4] > -1 and walls[z][y+max(0,j)][x][0][0][1] == 0 and (z == 0 or grid[z][y][x][2] != -1)) and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y+j][x][4] > 1):
                                    grid[z][y][x][4] = grid[z][y+j][x][4] + 1
                                elif (z + j >= 0 and z + j < len(grid)) and (grid[z+j][y][x][4] > -1 and grid[z][y][x][2] == 2 and grid[z+j][y][x][2] == 2) and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y+j][x][4] > 1):
                                    grid[z][y][x][4] = grid[z+j][y][x][4] + 1
                                
                                #if there is still an unreached tile left, set i to 1 and add 1 to the amount of unreached tiles count
                                if grid[0][y][x][4] == -1:
                                    i = 1
                                    count += 1
                        else:
                            #check for adjacent tiles whether: 
                            #   it is in the grid
                            #   the adjacent tile has a distance larger than -1
                            #   the current tile has a distance of -1 or that the difference between the distance of the current tile and adjacent tiles is larger than 1
                            for j in adjacent:
                                if (x + j >= 0 and x + j < len(grid[z][y])) and grid[z][y][x+j][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y][x+j][4] > 1):
                                    grid[z][y][x][4] = grid[z][y][x+j][4] + 1
                                elif (y + j >= 0 and y + j < len(grid[z])) and grid[z][y+j][x][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z][y+j][x][4] > 1):
                                    grid[z][y][x][4] = grid[z][y+j][x][4] + 1
                                elif (z + j >= 0 and z + j < len(grid)) and grid[z+j][y][x][4] > -1 and (grid[z][y][x][4] == -1 or grid[z][y][x][4] - grid[z+j][y][x][4] > 1):
                                    grid[z][y][x][4] = grid[z+j][y][x][4] + 1
                                
                                #if there is still an unreached tile left, set i to 1 (all tiles are reachable so unreached tiles are not counted)
                                if grid[z][y][x][4] == -1:
                                    i = 1
        i += 1
        #add 1 to the iteration count and check if 100 iterations are done
        iterationCount += 1
        if iterationCount == 100:
            print(str(int(count/(2*len(grid)))) + " first floor tiles could not be reaced within 100 iterations")
            break

    #set the distance of every tile with a distance of -1 to the maximum possible distance
    maxDistance = len(grid) * len(grid[0]) * len(grid[0][0])
    for z in range(len(grid)):
        for y in range(len(grid[z])):
            for x in range(len(grid[z][y])):
                if grid[z][y][x][4] == -1:
                    grid[z][y][x][4] = maxDistance
    
    return True, grid

def importFunctionFromCSV(grid, u, v):
    #import results from the block scale configuration
    abs_path = Path(os.path.realpath(__file__))
    parent_path = str(abs_path.parent.parent)

    for z in range(len(grid)):
        with open(parent_path + '/Block/functionsLevel' + str(z) + '.csv', 'r') as blockFunctions:
            reader = csv.reader(blockFunctions)
            y = 0
            rowIndex = 0

            for row in reader:
                if len(row) > 0:
                    x = 0

                    if rowIndex >= v:
                        for item in row:
                            #every tile in the block scale game are 5x5 tiles in the building scale game
                            for i in range(5):
                                for j in range(5):
                                    if 5*y+i < len(grid[z]) and 5*x+j < len(grid[z][y]):
                                        grid[z][5*y+i][5*x+j][6] = int(row[x + u])
                            x += 1
                        y += 1
                    rowIndex += 1
    return grid

def drawGrid(level, drawDistance, grid, walls, currentType, functionColours):
    #set a size in pixels for each tile
    blockSize = 50

    if len(walls) == 0:
        for z in range(len(grid)):
            walls.append([])
            if len(grid[z]) == 0:
                #define grid and wall rows for each row
                for y in range(0, WINDOW_HEIGHT - 2, blockSize):
                    walls[z].append([])
                    row = []

                    #define tiles and wall pairs for each tile
                    for x in range(0, WINDOW_WIDTH - 200, blockSize):
                        walls[z][int(y/blockSize)].append([])
                        rect = pygame.Rect(x, y, blockSize, blockSize)

                        #each wall has data on:
                        #   its starting and end point
                        #   whether it is closed or open
                        wallHorizontal = [((x, y), (x + blockSize, y)), 0]
                        wallVertical = [((x, y), (x, y + blockSize)), 0]
                        walls[z][int(y/blockSize)][int(x/blockSize)].append([wallHorizontal, wallVertical])

                        #define only a horizontal wall for the bottom most row of walls
                        if int(y/blockSize + 1) == len(walls[z]):
                            walls[z].append([])

                        if y + blockSize == WINDOW_HEIGHT - 2:
                            walls[z][int((y+blockSize)/blockSize)].append([])
                            wallHorizontal = [((x, y + blockSize), (x + blockSize, y + blockSize)), 0]
                            walls[z][int((y+blockSize)/blockSize)][int(x/blockSize)].append([wallHorizontal, -1])


                        #define only a vertical wall for the right most column of walls
                        if x + blockSize == WINDOW_WIDTH - 200:
                            wallVertical = [((x + blockSize, y), (x + blockSize, y + blockSize)), 0]
                            walls[z][int(y/blockSize)].append([[-1, wallVertical]])

                        #each tile has data on:
                        #   its visual rectangle
                        #   whether it is selected
                        #   its room function
                        #   its id number for its room function
                        #   its distance
                        #   its influence factor
                        #   its building type from the block scale game
                        row.append([rect, False, -1, 0, -1, 0, -1])
                    grid[z].append(row)
    
    for y in range(len(grid[level])):
        for x in range(len(grid[level][y])):
            item = grid[level][y][x]
            rect = item[0]
            buildingID = item[3]

            #if distances should not be drawn, each tile will have its colour according to its function or to whether it is selected. Building type colours are drawn darker
            if drawDistance == False:
                if item[2] != -1:
                    color = functionColours[currentType][item[2]]
                else:
                    color = (0.3 * funcColourDict[item[6]][0], 0.3 * funcColourDict[item[6]][1], 0.3 * funcColourDict[item[6]][2])
                pygame.draw.rect(SCREEN, color, rect)
                if item[1] == True:
                    pygame.draw.rect(SCREEN, RED, rect)
                text = font.render(str(buildingID), True, WHITE)

            else:
                maxDistance = len(grid) + len(grid[0]) + len(grid[0][0])
                colorIndex = max(0, int((maxDistance - item[4]*(maxDistance/10))/maxDistance * 125))

                #negative scores are drawn red, positive scores are drawn green, neutral scores are drawn cyan
                if item[5] < 0:
                    color = (min(255, -item[5] * colorIndex), 0, 0.1*colorIndex)
                elif item[5] > 0:
                    color = (0, min(255, item[5] * colorIndex), 0.3*colorIndex)
                else:
                    color = (0, colorIndex, colorIndex)

                
                pygame.draw.rect(SCREEN, color, rect)
                if item[1] == True:
                    pygame.draw.rect(SCREEN, RED, rect)
                text = font.render(str(buildingID), True, WHITE)
                text = font.render(str(item[4]), True, WHITE)

            #draw walls and colour them according to their openness
            for wall in walls[level][y][x]:
                if wall[0][1] == 0:
                    color = WHITE
                else:
                    color = RED
                pygame.draw.line(SCREEN, color, wall[0][0][0], wall[0][0][1], 2)
                if wall[1][1] == 0:
                    color = WHITE
                else:
                    color = RED
                pygame.draw.line(SCREEN, color, wall[1][0][0], wall[1][0][1], 2)

            if x + 1 == len(grid[level][y]):
                for wall in walls[level][y][x+1]:
                    if wall[1][1] == 0:
                        color = WHITE
                    else:
                        color = RED
                    pygame.draw.line(SCREEN, color, wall[1][0][0], wall[1][0][1], 2)

            if y + 1 == len(grid[level]):
                for wall in walls[level][y+1][x]:
                    if wall[0][1] == 0:
                        color = WHITE
                    else:
                        color = RED
                    pygame.draw.line(SCREEN, color, wall[0][0][0], wall[0][0][1], 2)

            SCREEN.blit(text, (rect[0], rect[1]))
    return grid

def placeFunction(currentType, size, p, grid, functionID, allowed, built, walls):
    level = p[2]
    gridCopy = copy.deepcopy(grid)
    wallsCopy = copy.deepcopy(walls)
    
    #iterate through all the tiles that will be built
    for row in range(p[1],p[1]+size[currentType][functionID][1]):
        if row >= 0 and row < len(gridCopy[level]):
            for item in range(p[0],p[0]+size[currentType][functionID][0]):

                #check whether current tile is in grid, whether a room type is selected (and is not an entrance), whether the tile is empty, and whether the tile is withing the right building type
                if item >= 0 and item < len(gridCopy[level][row]) and functionID > 0 and gridCopy[level][row][item][2] == -1 and gridCopy[level][row][item][6] == currentType:

                    #built on the current floor and next floor if a staircase is placed
                    if functionID == 2:
                        if level + 1 < len(gridCopy):
                            for floor in range(level, level + 2):
                                gridCopy[floor][row][item][2] = functionID
                                gridCopy[floor][row][item][3] = built[currentType][functionID]

                                if row == p[1]:
                                    wallsCopy[floor][row][item][0][0][1] = 1
                                if row == p[1] + size[currentType][functionID][1] - 1:
                                    wallsCopy[floor][row + 1][item][0][0][1] = 1
                                if item == p[0]:
                                    wallsCopy[floor][row][item][0][1][1] = 1
                                if item == p[0] + size[currentType][functionID][0] - 1:
                                    wallsCopy[floor][row][item + 1][0][1][1] = 1
                    
                    #built on the current floor if a staircase is placed
                    else:
                        gridCopy[level][row][item][2] = functionID
                        gridCopy[level][row][item][3] = built[currentType][functionID]
                    
                        if row == p[1]:
                            wallsCopy[level][row][item][0][0][1] = 1
                        if row == p[1] + size[currentType][functionID][1] - 1:
                            wallsCopy[level][row + 1][item][0][0][1] = 1
                        if item == p[0]:
                            wallsCopy[level][row][item][0][1][1] = 1
                        if item == p[0] + size[currentType][functionID][0] - 1:
                            wallsCopy[level][row][item + 1][0][1][1] = 1

                #if an entrance is selected to be built, check whether the selected tile is either a road or public courtyard tile  
                elif functionID == 0 and gridCopy[level][row][item][2] == -1 and (gridCopy[level][row][item][6] == 5 or gridCopy[level][row][item][6] == 6):
                    gridCopy[level][row][item][2] = functionID
                    gridCopy[level][row][item][3] = built[currentType][functionID]
                else:
                    return grid, walls, allowed, built
        else:
            return grid, walls, allowed, built

    #recalculate allowed rooms and built rooms
    allowed[currentType][functionID] -= 1
    built[currentType][functionID] += 1
    return gridCopy, wallsCopy, allowed, built

main()
