# EARTHY4.0G7 - Building scale game
### Written by Kaan Akbaba
### Last revision: 02/11/2021

## SETTING UP THE GAME
First you need to install pygame in vscode by entering the following command into the terminal:  
- py -m pip install -U pygame --user
  
If that doesn't work, try installing the latest python version from https://www.python.org/downloads/ before installing pygame; this solved issues for me, at least. Let me know if any issues persist for you.  

## GAME CONTROLS
This game is a continuation of the block scale game. After playing the block scale game, two .csv files are created that are used in this game. This game zooms in to a location of the previously made configuration. The location that is zoomed into can only be changed within the code as of now.  
First the player needs to select which building type they are designing. After choosing one, all the rooms for this type of building are presented, with the amount of allowed number of each room type next to it.  
It is important to note that rooms can only be placed in areas with the correct building type (indicated with a dimmer colour). The colours for each building type are:  
- Green for residential
- Blue for shops
- Purple for restaurants
- Orange for workshops
- Cyan for creative workshops
  
When a room type and tile are selected, pressing the b key places this room down. Note that it can only be placed when there are no obstructions in the entire room area, and when the entire room area is inside the correct building type. The selected tile will always be at the top left corner of a placed room.  
After a room is placed, the walls around it are all closed (indicated by red). Walls can be toggled between an open (white) and closed state. This is necessary to calculate the connectivity between rooms. The keys to do this are:  
1. toggle wall on top of selected tile
2. toggle wall right of selected tile
3. toggle wall below the selected tile
4. toggle wall left of selected tile
   
The entrance tiles of the building should also be indicated. This is necessary for the adjacency scoring. Note that entrance tiles can only be placed on roads or public courtyards (block scale!).  
  
By pressing the up and down key, the player can alternate between levels.  
  
At every moment in the game relations between rooms can be inspected. The relations considered are:  
- Spatial distance (d)
- Connectivity (c)
- Adjacency (a)
  
Between brackets is indicated which key to press to inspect this relation. Be sure to have a room selected before inspecting one of these relations!  
The colours while inspecting the interfunctional relations indicate whether the influence is positive (green), negative (red), or neutral (cyan). The dimmer the colour, the lower the influence.  
By pressing the ENTER key the total score for the entire configuration is calculated and displayed.  
  
Have fun and let me know what you think!  