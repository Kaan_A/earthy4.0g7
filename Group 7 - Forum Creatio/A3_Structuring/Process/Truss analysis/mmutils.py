import numpy as np
import math

# Based on a script from a python workshop from the course CIE4190
# Made by Kaan Akbaba
# Last revised: 02/11/2021

def read_input (fname,dofs_per_node):
    nodes     = []
    elems     = []
    mode      = ""
    dof_count = 0

    with open (fname) as inp:
        for line in inp:
            if 'nodes' in line:
                mode = 'node'
            elif 'elements' in line:
                mode = 'elem'
            else:
                split = line.split()
                if len(split) == 0:
                    continue
                if mode == 'node':
                    node  = {}
                    node['x'] = float(split[0])
                    node['y'] = float(split[1])
                    node['z'] = float(split[2])
                    node['Fx'] = float(split[3])
                    node['Fy'] = float(split[4])
                    node['Fz'] = float(split[5])
                    node['dofs'] = [dof_count+i for i in range(dofs_per_node)]
                    dof_count = dof_count + dofs_per_node
                    nodes.append (node)
                if mode == 'elem':
                    elem  = {}
                    elem['i'] = nodes[int(split[0])]
                    elem['j'] = nodes[int(split[1])]
                    elem['E'] = float(split[2])
                    elem['q']  = float(split[3])
                    elems.append (elem)

    return nodes, elems, dof_count

def get_constrained_dofs(fname):
    cdofs = []
    with open(fname) as inp:
        for line in inp:
            split = line.split()
            if len(split) != 0:
                cdofs.append(int(split[0]))
    return cdofs


def rotation_matrix(element):
    n1 = element['i']
    n2 = element['j']

    dx = n2['x'] - n1['x']
    dy = n2['y'] - n1['y']
    dz = n2['z'] - n1['z']

    rz = np.zeros((6,6))
    ry = np.zeros((6,6))

    if dx != 0.0:
        a = math.atan(dy/dx)
    elif dy != 0.0:
        a = dy/abs(dy) * 0.5*math.pi
    else:
        a = 0.0
    
    Lxy = math.cos(a)*dx + math.sin(a)*dy

    if Lxy != 0.0:
        b = -math.atan(dz/Lxy)
    elif dz != 0.0:
        b = dz/abs(dz) * -0.5*math.pi
    else:
        b = 0.0
    
    ey = np.zeros(3)
    raxis = np.zeros((3,3))
    ey[1] = 1.0

    raxis[0,0] = raxis[1,1] = math.cos(a)
    raxis[0,1] = -math.sin(a)
    raxis[1,0] = math.sin(a)
    raxis[2,2] = 1.0

    rz[0,0] = rz[1,1] = rz[3,3] = rz[4,4] = math.cos(a)
    rz[0,1] = rz[3,4] = math.sin(a)
    rz[1,0] = rz[4,3] = -math.sin(a)
    rz[2,2] = rz[5,5] = 1.0

    u = np.matmul(raxis, ey)

    ry[0,0] = ry[3,3] = math.cos(b) + (u[0]**2)*(1 - math.cos(b))
    ry[0,1] = ry[1,0] = ry[3,4] = ry[4,3] = u[0]*u[1]*(1 - math.cos(b))
    ry[0,2] = ry[3,5] = -u[1]*math.sin(b)
    ry[1,1] = ry[4,4] = math.cos(b) + (u[1]**2)*(1 - math.cos(b))
    ry[1,2] = ry[4,5] = u[0]*math.sin(b)
    ry[2,0] = ry[5,3] = u[1]*math.sin(b)
    ry[2,1] = ry[5,4] = -u[0]*math.sin(b)
    ry[2,2] = ry[5,5] = math.cos(b)
    
    r = np.matmul(rz,ry)
    return r

def truss_stiffness(element):
    k_ele = np.zeros((6,6))

    n1 = element['i']
    n2 = element['j']

    dx = n2['x'] - n1['x']
    dy = n2['y'] - n1['y']
    dz = n2['z'] - n1['z']

    L = np.sqrt(dx**2.0 + dy**2.0 + dz**2.0)

    if L == 0:
        print(element)
    
    r = rotation_matrix(element)
    rt = np.transpose(r)

    EA = element['E'] * 0.05 * 0.1
    
    k_ele[0,0] = k_ele[3,3] = EA/L
    k_ele[0,3] = k_ele[3,0] = -EA/L
    
    k_global = np.matmul (np.matmul (rt, k_ele), r)

    return k_global

def truss_load (element):
    n1 = element['i']
    n2 = element['j']
    q  = element['q']

    dx = n2['x'] - n1['x']
    dy = n2['y'] - n1['y']
    dz = n2['z'] - n1['z']

    L = np.sqrt(dx**2.0 + dy**2.0 + dz**2.0)
    
    r = rotation_matrix(element)
    rt = np.transpose(r)

    f_ele = np.zeros(6)

    f_ele[0] = f_ele[2] = 0.5*q*L

    f_global = np.matmul (rt,f_ele)

    return f_global